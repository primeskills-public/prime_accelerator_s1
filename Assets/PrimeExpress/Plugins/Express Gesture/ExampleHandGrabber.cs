using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExampleHandGrabber : MonoBehaviour
{
    public float distance_offscreen = 1f;

    // Update is called once per frame
    void FixedUpdate()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        transform.position = ray.origin + ray.direction * distance_offscreen;
    }
}
