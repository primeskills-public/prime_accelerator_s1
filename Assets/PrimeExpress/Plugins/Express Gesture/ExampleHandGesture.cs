using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class ExampleHandGesture : MonoBehaviour
{
    public float distance_offscreen = 1f;
    public Animator animator;
    public string animation_param = "Gesture";

    [Header("Misc Options")]
    public bool disableOnStart = true;

    private void Start()
    {
        gameObject.SetActive(!disableOnStart);
    }


    // Update is called once per frame
    void FixedUpdate()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        transform.position = ray.origin + ray.direction * distance_offscreen;
    }

    private void Update()
    {
        if(Input.GetKey(KeyCode.Mouse0))
        {
            animator.SetBool(animation_param, true);
        } else
        {
            animator.SetBool(animation_param, false);
        }
    }
}
