﻿using System;
using UnityEngine;

namespace Weelco.SpeechControl
{
    public class MicInput_Weelco : MonoBehaviour
    {
        // public Slider soundLevelBar;
        public static float MicLoudness = 0;
        public bool debugMicLevel = false;

        private string _device;

        private AudioClip _clipRecord;
        // UserDataTracker userDataTracker;
        private static bool created = false;

        public SavWav_Aldo savWav_aldo;
        public int record_frequency = 44100;
        public float _soundLevel { get; private set; } = 0f;

        private void Awake()
        {
            _clipRecord = AudioClip.Create("clip", 128, 1, 44100, true);
            savWav_aldo = GetComponent<SavWav_Aldo>();
            //if (!created)
            //{
            //    DontDestroyOnLoad(this.gameObject);
            //    created = true;
            //    Debug.Log("Awake: " + this.gameObject);
            //}  else
            //{
            //    Destroy(this.gameObject);
            //}
        }

        //mic initialization
        void InitMic()
        {
            foreach (var device in Microphone.devices)
            {
                Debug.Log("Name Mic: " + device);
            }
            if (_device == null) _device = Microphone.devices[0];
            Debug.Log("Selected Mic: " + _device);
            _clipRecord = Microphone.Start(_device, true, 999, record_frequency); //change 999
        }

        public bool isRecording()
        {
            if (_device == null) return false;
            return Microphone.IsRecording(_device);
        }

        public void StartRecording(int lengthSecond = 120)
        {
            if (Microphone.IsRecording(_device))
            {
                StopMicrophone();
                _clipRecord = null;
            }
            _clipRecord = Microphone.Start(_device, true, lengthSecond, record_frequency); //change 120
            Debug.Log("started");
        }

        public void StopMicrophone()
        {
            string path = Application.streamingAssetsPath + "/file2.wav";
            Debug.Log(path);
            Microphone.End(_device);
        }

        public void SaveRecording(string fileName = "")
        {
            if (fileName.Length == 0) fileName = DateTime.Now.ToString("yyyyMMddHHmmss");
            if (!savWav_aldo) savWav_aldo = GetComponent<SavWav_Aldo>();
            savWav_aldo.Save(fileName, _clipRecord);
        }

        public void SaveRecording(AudioClip audioClip, string fileName = "")
        {
            if (fileName.Length == 0) fileName = DateTime.Now.ToString("yyyyMMddHHmmss");
            SavWav_Aldo s = GetComponent<SavWav_Aldo>();
            s.Save(fileName, audioClip);
        }

        public AudioClip ReturnClipRecord()
        {
            return _clipRecord;
        }

        public AudioClip ReturnCopyClipRecord(float saveTime = 0f)
        {
            Debug.Log("saveTime: " + saveTime);
            int sampleSize = (saveTime > 0f) ? Mathf.CeilToInt((saveTime) * record_frequency) : _clipRecord.samples;

            AudioClip retClip = AudioClip.Create("clip-copy", sampleSize, _clipRecord.channels, _clipRecord.frequency, false);

            Debug.Log(string.Format("New Copy Sample Size: {0} || Ori Sample Size: {1}", sampleSize, _clipRecord.samples));

            // get samples from previous clip
            float[] samples = new float[sampleSize * _clipRecord.channels];
            _clipRecord.GetData(samples, 0);

            retClip.SetData(samples, 0);

            return retClip;
        }

        int _sampleWindow = 128;

        //get data from microphone into audioclip
        float LevelMax()
        {
            float levelMax = 0;
            float[] waveData = new float[_sampleWindow];
            int micPosition = Microphone.GetPosition(null) - (_sampleWindow + 1); // null means the first microphone
            if (micPosition < 0) return 0;
            _clipRecord.GetData(waveData, micPosition);
            // Getting a peak on the last 128 samples
            for (int i = 0; i < _sampleWindow; i++)
            {
                float wavePeak = waveData[i] * waveData[i];
                if (levelMax < wavePeak)
                {
                    levelMax = wavePeak;
                }
            }
            return levelMax;
        }

        public float scale(float OldMin, float OldMax, float NewMin, float NewMax, float OldValue)
        {
            float OldRange = (OldMax - OldMin);
            float NewRange = (NewMax - NewMin);
            float NewValue = (((OldValue - OldMin) * NewRange) / OldRange) + NewMin;

            return (NewValue);
        }

        void FixedUpdate()
        {
            // levelMax equals to the highest normalized value power 2, a small number because < 1
            // pass the value to a static var so we can access it from anywhere
            try
            {
                MicLoudness = LevelMax();
            }
            catch
            {
                MicLoudness = 0;
            }
            _soundLevel = scale(0, 10, 0, 1, MicLoudness * 1000);

            if (debugMicLevel)
                Debug.Log("sound level: " + _soundLevel);
            // soundLevelBar.value = soundLevel;
        }

        bool _isInitialized;
        // start mic when scene starts
        void OnEnable()
        {
            try
            {
                InitMic();
                _isInitialized = true;
            }
            catch
            {
                _isInitialized = false;
            }
        }

        //stop mic when loading a new level or quit application
        void OnDisable()
        {
            StopMicrophone();
        }

        void OnDestroy()
        {
            StopMicrophone();
        }

        // make sure the mic gets started & stopped when application gets focused
        void OnApplicationFocus(bool focus)
        {
            if (focus)
            {
                Debug.Log("Focus");

                if (!_isInitialized)
                {
                    Debug.Log("Init Mic");
                    try
                    {
                        InitMic();
                        _isInitialized = true;
                    }
                    catch
                    {
                        _isInitialized = false;
                    }
                }
            }
            if (!focus)
            {
                Debug.Log("Pause");
                StopMicrophone();

                _isInitialized = false;

            }
        }
    }
}