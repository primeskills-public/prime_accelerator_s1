using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR;
using Weelco.SpeechControl;
using Prime.Express;

public class B_QTA_AudioBodyPlayBack : BasicEvent
{
    [Header("Player Stuff")]
    //public cs_Fade_In_Out FadeInOut_Player;
    //public cs_TeleportCharacter cs_Teleport;
    //public Controller_Face_Movement Player_Face_Controller;
    public AudioSource playerAudioSource;

    //[Header("IK-STUFF")]
    //public VRIK VrIkPlayer;
    //public Prime_IK_PlayerFull Prime_IK_Player;
    //public Prime_IK_RecorderFull Prime_IK_Recorder;

    [Header("Jump To Position")]
    public Transform TeleportPoint;
    public Transform OriginalPosition;
    public GameObject TempCharacter;
    public float DelayStart = 0.5f;
    private Transform cs_sit;

    public override IEnumerator Flow_Initiate()
    {     
        return base.Flow_Initiate();
    }
  
    public override IEnumerator Flow_EventSequence()
    {
        //delay start
        yield return new WaitForSeconds(DelayStart);
       
        // Fade out screen prepare for teleport
        //FadeInOut_Player.ScreenFadeOut();
        //yield return new WaitUntil(() => FadeInOut_Player.GetFadeAlpha() == 1f);

        // turn off animation controller
        //if (VrIkPlayer.gameObject.GetComponent<Animator>()) VrIkPlayer.gameObject.GetComponent<Animator>().enabled = false;

        // switch mouth controller input if any
        //if (Player_Face_Controller)
        //{
        //    Player_Face_Controller.sample_source = Controller_Face_Movement.SampleSource.AudioSource;
        //    Player_Face_Controller.as_sample_source = playerAudioSource;
        //}
        // wait until end of frame to safely disable VR IK
        //yield return new WaitForEndOfFrame();
        //VrIkPlayer.enabled = false;
       
        // teleport to the point
        //cs_Teleport.TeleportTo(TeleportPoint, null, false);

        // fade in in new position
        //FadeInOut_Player.ScreenFadeIn();
        //yield return new WaitUntil(() => FadeInOut_Player.GetFadeAlpha() == 0f);

        // wait for stability
        yield return new WaitForSeconds(1f);

        // loading recorded audio clip from
        playerAudioSource.clip = B_QTA_Recording.recordContainer[B_QTA_Recording.recordContainer.Count - 1];

        // Play back IK recording and audio at the same time
        //Prime_IK_Player.InitiatePlay(Prime_IK_Recorder.GetDollRecord());
        playerAudioSource.Play();
        Debug.Log($"Audio Source harusnya memutar sebuah Audio!");

        // wait one frame for stability, then wait until ik player is finish playing
        yield return null;
        //yield return new WaitUntil(() => !Prime_IK_Player.isPlaying);
        yield return new WaitUntil(() => playerAudioSource.isPlaying == false);
        Debug.Log($"Audio Source telah selesai memutar Audio");
        //playerAudioSource.Stop();

        // Fade out screen prepare for teleport
        //FadeInOut_Player.ScreenFadeOut();
        //yield return new WaitUntil(() => FadeInOut_Player.GetFadeAlpha() == 1f);

        // teleport to original position 
        //cs_Teleport.TeleportTo(OriginalPosition, null, false);

        // swicth mouth controller input back to microphone
        //if (Player_Face_Controller) Player_Face_Controller.sample_source = Controller_Face_Movement.SampleSource.Microphone;

        // wait until end of frame to safely enabled VR IK
        //yield return new WaitForEndOfFrame();
        //VrIkPlayer.enabled = true;

        // turn on animation controller
        //if (VrIkPlayer.gameObject.GetComponent<Animator>()) VrIkPlayer.gameObject.GetComponent<Animator>().enabled = false;

        // clear clip
        playerAudioSource.clip = null;

        // fade in new position
        //FadeInOut_Player.ScreenFadeIn();
        //yield return new WaitUntil(() => FadeInOut_Player.GetFadeAlpha() == 0f);

        // wait for stability
        yield return new WaitForSeconds(2f);

        // getting reference to IK player and recorder objeck

    }

    public override IEnumerator Flow_Terminate()
    {
        return base.Flow_Terminate();
    }
}

