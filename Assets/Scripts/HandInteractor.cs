using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Prime.Express;
using UnityEngine.XR.Interaction.Toolkit;

public class HandInteractor : BaseDirectGrabbableController
{
    public ActionBasedController leftHand;
    public ActionBasedController rightHand;

    protected override bool GetGrabAttempt()
    {
        bool retVal = false;
        switch (hand)
        {
            case Hand.Left:
                retVal = Input.GetKeyDown(KeyCode.Space) || leftHand.activateAction.action.phase == UnityEngine.InputSystem.InputActionPhase.Started;
                break;
            case Hand.Right:
                retVal = Input.GetKeyDown(KeyCode.Space) || rightHand.activateAction.action.phase == UnityEngine.InputSystem.InputActionPhase.Started;
                break;
        }

        Debug.Log($"{retVal}");
        return retVal;
    }

    protected override bool CheckTriggerDownState()
    {
        // define the return value of each hand
        bool retVal = false;
        switch (hand)
        {
            case Hand.Left:
                retVal = Input.GetKey(KeyCode.Space) || leftHand.activateAction.action.phase == UnityEngine.InputSystem.InputActionPhase.Performed;
                break;
            case Hand.Right:
                retVal = Input.GetKey(KeyCode.Space) || rightHand.activateAction.action.phase == UnityEngine.InputSystem.InputActionPhase.Performed;
                break;
        }

        Debug.Log($"{retVal}");
        return retVal;
    }
}
