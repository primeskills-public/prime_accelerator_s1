using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit;

public class LeftHandReturnSample : MonoBehaviour
{
    [SerializeField] private ActionBasedController _leftHandController;

    private void Update()
    {
        if(_leftHandController.activateAction.action.phase == InputActionPhase.Performed)
        {
            Debug.Log($"Trigger pressed!");
        }

        Debug.Log($"Trigger released.");
    }
}
