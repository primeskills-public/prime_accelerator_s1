using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Canvas_Fader : MonoBehaviour
{
    #region Main Variable & Reference
    [Header("Main Component")]
    public GameObject selfGO;
    public CanvasGroup selfCG;

    [Header("Canvas Status")]
    public FADE_STATUS fadeStatus = FADE_STATUS.IDLE;
    public float speedMultiplier;

    public enum FADE_STATUS
    {
        IDLE,
        FADE_IN,
        FADE_OUT
    }
    #endregion

    #region MonoBehaviour
    private void Start()
    {
        
    }
    #endregion

    #region Canvas Fader
    public void Initiate(FADE_STATUS _fadeStatus, float _multiplier = 1.0f)
    {
        StartCoroutine(CR_FADING(_fadeStatus, _multiplier));
    }
    #endregion

    #region DEBUG
    [ContextMenu("FADE_IN")]
    public void FadeIn()
    {
        //StartCoroutine(CR_FADING(FADE_STATUS.FADE_IN, Random.Range(0.3f, 3.1f)));
        StartCoroutine(CR_FADING(FADE_STATUS.FADE_IN, 1f));
    }

    [ContextMenu("FADE_OUT")]
    public void FadeOut()
    {
        //StartCoroutine(CR_FADING(FADE_STATUS.FADE_OUT, Random.Range(0.3f, 3.1f)));
        StartCoroutine(CR_FADING(FADE_STATUS.FADE_OUT, 1f));
    }
    #endregion

    #region Coroutine
    IEnumerator CR_FADING(FADE_STATUS _fadeStatusCR, float multiplier)
    {
        speedMultiplier = multiplier;

        switch (_fadeStatusCR)
        {
            case FADE_STATUS.FADE_IN:
                this.fadeStatus = FADE_STATUS.FADE_IN;
                while(selfCG.alpha < 1)
                {
                    selfCG.alpha += 0.01f * multiplier;
                    yield return null;
                }
                break;
            case FADE_STATUS.FADE_OUT:
                this.fadeStatus = FADE_STATUS.FADE_OUT;
                while (selfCG.alpha > 0)
                {
                    selfCG.alpha -= 0.01f * multiplier;
                    yield return null;
                }
                break;
            default:
                Debug.LogError($"Case tidak ditemukan!");
                break;
        }

        this.fadeStatus = FADE_STATUS.IDLE;
    }
    #endregion
}
