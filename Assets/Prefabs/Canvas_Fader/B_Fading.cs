using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Prime.Express;

public class B_Fading : BasicEvent
{
    #region Main Variable & Reference
    [Header("Main Reference")]
    public Canvas_Fader canvasFader;

    [Header("Canvas Fader Setting")]
    public Canvas_Fader.FADE_STATUS fadeStatus = Canvas_Fader.FADE_STATUS.IDLE;
    [Range(0.25f, 2.0f)] public float multiplier = 0.25f;
    #endregion

    #region BasicEvent
    public override IEnumerator Flow_EventSequence()
    {
        if (fadeStatus == Canvas_Fader.FADE_STATUS.IDLE)
        {
            Debug.Log($"PERIKSA KEMBALI fadeStatus! IDLE TIDAK TERJADI APAPUN!");
        }
        else
        {
            canvasFader.Initiate(fadeStatus, multiplier);
        }

        yield return new WaitForSeconds(1f);
        yield return new WaitUntil(() => canvasFader.fadeStatus == Canvas_Fader.FADE_STATUS.IDLE);
    }
    #endregion
}
